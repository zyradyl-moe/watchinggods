---
date: 2020-05-17T15:45:00Z
lastmod: 2020-05-18T15:45:00Z
publishdate: 2020-05-17T15:45:00Z
draft: false
weight: 9

title: "Corporations"
description: "NeoBabel Global Corporations"
---

Ever since the [Corporate Assistance for the Survival of Humanity (CASH) Act][1]
of <span style="color:red">????</span> corporations have played a vital role in
the public's life. While there are an infinite number of corporations present on
Earth, not to mention the additional hordes of startups and small businesses,
there are only a few corporations so large as to be immediately recognizable to
most of the general public.

## [Global Engineering & Protection Corporation (GEP Co.)][2]

Responsible for the
[Climate Analysis, Control, Engineering, and Protection Systems (CACEPS)][3],
GEP Corporation is perhaps the company with the largest direct responsibility
for the survival of the human race. However, due to the massive complexity and
cost involved in constructing new CACEPS, GEP Corporation has remained
fundamentally the same company since it was spun off from the Government
MegaProjects of the past.

## [Human Aptitude and Longevity Optimization Corporation (HALOC)][4]

Responsible for production of the [Babel Implant][5],
[Subcutaneous Chemical Administration Port][6], and the administration of the
[Human Aptitude Augmentation and Reporting System (HAARPS)][7], HALOC is the
corporation that the general population has the largest direct contact with. Due
to the extremely large domain of influence HALOC has, it is one of the few
corporations that still reports regularly to the [Unified Human Government][8].
Shockingly, despite almost every individual in the world having received both
the Babel Implant and the SCAP, ***there has never been even a single report
of an adverse effect.***

## [Human Accountability Institute (HAI)][9]

The Human Accountability Institute serves a dual purpose as both a labor broker
and a welfare broker. HAI is mandated to ensure that corporations have access to
those with valuable skills, those with sufficient skills have access to
employment, and those without employment have access to sufficient resources
for survival. As such, HAI administers the global talent pool, assists with
cross-city relocation where applicable, and administers the governmentally
mandated basic income. To facilitate easy delivery of both jobs and funds,
every citizen is assigned a [Human Routing Number (HRN)][10] at birth.

[1]: /laws/cash/
[2]: /corporations/gep/
[3]: /technology/caceps/
[4]: /corporations/haloc/
[5]: /technology/babel/
[6]: /technology/scap/
[7]: /laws/haarps/
[8]: /government/uhg/
[9]: /corporations/hai/
[10]: /laws/hrn/
